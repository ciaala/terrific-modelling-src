# CMake generated Testfile for 
# Source directory: /home/crypt/projects/terrificmodelling/src/test
# Build directory: /home/crypt/Documents/src/test
# 
# This file includes the relevent testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
ADD_TEST(test_quaternion "/test_quaternion")
ADD_TEST(test_tctreemodel "/test_tctreemodel")
ADD_TEST(test_vector "/test_vector")
