#include <kdialog.h>
#include <klocale.h>

/********************************************************************************
** Form generated from reading UI file 'console.ui'
**
** Created: Mon Dec 20 17:06:41 2010
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONSOLE_H
#define UI_CONSOLE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDockWidget>
#include <QtGui/QHeaderView>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_console
{
public:
    QWidget *dockWidgetContents;

    void setupUi(QDockWidget *console)
    {
        if (console->objectName().isEmpty())
            console->setObjectName(QString::fromUtf8("console"));
        console->resize(400, 300);
        dockWidgetContents = new QWidget();
        dockWidgetContents->setObjectName(QString::fromUtf8("dockWidgetContents"));
        console->setWidget(dockWidgetContents);

        retranslateUi(console);

        QMetaObject::connectSlotsByName(console);
    } // setupUi

    void retranslateUi(QDockWidget *console)
    {
        console->setWindowTitle(tr2i18n("Console", 0));
    } // retranslateUi

};

namespace Ui {
    class console: public Ui_console {};
} // namespace Ui

QT_END_NAMESPACE

#endif // CONSOLE_H

