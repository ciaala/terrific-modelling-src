#include <kdialog.h>
#include <klocale.h>

/********************************************************************************
** Form generated from reading UI file 'terrificmodellingview_base.ui'
**
** Created: Tue Dec 21 10:41:46 2010
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TERRIFICMODELLINGVIEW_BASE_H
#define UI_TERRIFICMODELLINGVIEW_BASE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDoubleSpinBox>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPlainTextEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTabWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "kcolorbutton.h"
#include "terrificcoder/tccolorbutton.h"
#include "terrificcoder/tcglwidget.h"
#include "terrificcoder/tclistview.h"

QT_BEGIN_NAMESPACE

class Ui_terrificmodellingview_base
{
public:
    QGridLayout *gridLayout;
    QWidget *widget2;
    QVBoxLayout *verticalLayout;
    QTabWidget *tabWidget_2;
    QWidget *tab_3;
    QVBoxLayout *verticalLayout_2;
    QPushButton *pushButton_6;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QPushButton *pushButton_4;
    QPushButton *pushButton_8;
    QPushButton *pushButton_5;
    QSpacerItem *verticalSpacer;
    QWidget *tab_4;
    QHBoxLayout *horizontalLayout;
    TerrificCoder::TCListView *cameraListView;
    QWidget *tab_6;
    QVBoxLayout *verticalLayout_3;
    QPushButton *pushButton_7;
    TerrificCoder::TCListView *objectListView;
    QTabWidget *tabWidget;
    QWidget *tab;
    QHBoxLayout *horizontalLayout_2;
    QPlainTextEdit *console;
    QWidget *tab_2;
    QLabel *label_2;
    QLabel *label_5;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_6;
    QLabel *label_7;
    QWidget *tab_5;
    QGridLayout *gridLayout_3;
    QDoubleSpinBox *doubleSpinBox;
    QDoubleSpinBox *doubleSpinBox_2;
    QDoubleSpinBox *doubleSpinBox_3;
    QLabel *label;
    QLabel *label_8;
    QDoubleSpinBox *doubleSpinBox_4;
    QDoubleSpinBox *doubleSpinBox_5;
    QDoubleSpinBox *doubleSpinBox_6;
    QLabel *label_9;
    QLineEdit *lineEdit;
    QSpacerItem *verticalSpacer_2;
    TerrificCoder::TCColorButton *colorbutton;
    QLabel *label_10;
    QLabel *label_11;
    QLabel *label_12;
    QWidget *tab_7;
    QGridLayout *gridLayout_2;
    QLabel *label_13;
    TerrificCoder::TCColorButton *colorbutton_2;
    QLabel *label_14;
    TerrificCoder::TCColorButton *colorbutton_3;
    QLabel *label_16;
    TerrificCoder::TCColorButton *colorbutton_5;
    QLabel *label_18;
    TerrificCoder::TCColorButton *colorbutton_6;
    QLabel *label_17;
    QDoubleSpinBox *doubleSpinBox_7;
    QDoubleSpinBox *doubleSpinBox_8;
    QDoubleSpinBox *doubleSpinBox_9;
    QSpacerItem *verticalSpacer_3;
    QLabel *label_15;
    QLabel *label_19;
    QDoubleSpinBox *doubleSpinBox_10;
    QDoubleSpinBox *doubleSpinBox_11;
    TerrificCoder::TCGLWidget *glwidget;

    void setupUi(QWidget *terrificmodellingview_base)
    {
        if (terrificmodellingview_base->objectName().isEmpty())
            terrificmodellingview_base->setObjectName(QString::fromUtf8("terrificmodellingview_base"));
        terrificmodellingview_base->resize(1440, 900);
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(1);
        sizePolicy.setVerticalStretch(1);
        sizePolicy.setHeightForWidth(terrificmodellingview_base->sizePolicy().hasHeightForWidth());
        terrificmodellingview_base->setSizePolicy(sizePolicy);
        terrificmodellingview_base->setMinimumSize(QSize(1440, 900));
        terrificmodellingview_base->setSizeIncrement(QSize(16, 9));
        terrificmodellingview_base->setBaseSize(QSize(1440, 900));
        terrificmodellingview_base->setAutoFillBackground(true);
        terrificmodellingview_base->setStyleSheet(QString::fromUtf8("color: rgb(48, 48, 48);"));
        gridLayout = new QGridLayout(terrificmodellingview_base);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        widget2 = new QWidget(terrificmodellingview_base);
        widget2->setObjectName(QString::fromUtf8("widget2"));
        sizePolicy.setHeightForWidth(widget2->sizePolicy().hasHeightForWidth());
        widget2->setSizePolicy(sizePolicy);
        verticalLayout = new QVBoxLayout(widget2);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(1, 1, 1, 1);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        tabWidget_2 = new QTabWidget(widget2);
        tabWidget_2->setObjectName(QString::fromUtf8("tabWidget_2"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(2);
        sizePolicy1.setVerticalStretch(1);
        sizePolicy1.setHeightForWidth(tabWidget_2->sizePolicy().hasHeightForWidth());
        tabWidget_2->setSizePolicy(sizePolicy1);
        tabWidget_2->setMovable(true);
        tab_3 = new QWidget();
        tab_3->setObjectName(QString::fromUtf8("tab_3"));
        verticalLayout_2 = new QVBoxLayout(tab_3);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        pushButton_6 = new QPushButton(tab_3);
        pushButton_6->setObjectName(QString::fromUtf8("pushButton_6"));

        verticalLayout_2->addWidget(pushButton_6);

        pushButton = new QPushButton(tab_3);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        verticalLayout_2->addWidget(pushButton);

        pushButton_2 = new QPushButton(tab_3);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        verticalLayout_2->addWidget(pushButton_2);

        pushButton_3 = new QPushButton(tab_3);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));

        verticalLayout_2->addWidget(pushButton_3);

        pushButton_4 = new QPushButton(tab_3);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));

        verticalLayout_2->addWidget(pushButton_4);

        pushButton_8 = new QPushButton(tab_3);
        pushButton_8->setObjectName(QString::fromUtf8("pushButton_8"));

        verticalLayout_2->addWidget(pushButton_8);

        pushButton_5 = new QPushButton(tab_3);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));

        verticalLayout_2->addWidget(pushButton_5);

        verticalSpacer = new QSpacerItem(20, 163, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);

        tabWidget_2->addTab(tab_3, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QString::fromUtf8("tab_4"));
        horizontalLayout = new QHBoxLayout(tab_4);
        horizontalLayout->setSpacing(0);
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        cameraListView = new TerrificCoder::TCListView(tab_4);
        cameraListView->setObjectName(QString::fromUtf8("cameraListView"));
        cameraListView->setEditTriggers(QAbstractItemView::NoEditTriggers);
        cameraListView->setUniformItemSizes(true);

        horizontalLayout->addWidget(cameraListView);

        tabWidget_2->addTab(tab_4, QString());
        tab_6 = new QWidget();
        tab_6->setObjectName(QString::fromUtf8("tab_6"));
        verticalLayout_3 = new QVBoxLayout(tab_6);
        verticalLayout_3->setSpacing(0);
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        pushButton_7 = new QPushButton(tab_6);
        pushButton_7->setObjectName(QString::fromUtf8("pushButton_7"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(1);
        sizePolicy2.setVerticalStretch(1);
        sizePolicy2.setHeightForWidth(pushButton_7->sizePolicy().hasHeightForWidth());
        pushButton_7->setSizePolicy(sizePolicy2);

        verticalLayout_3->addWidget(pushButton_7);

        objectListView = new TerrificCoder::TCListView(tab_6);
        objectListView->setObjectName(QString::fromUtf8("objectListView"));
        QSizePolicy sizePolicy3(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy3.setHorizontalStretch(1);
        sizePolicy3.setVerticalStretch(1);
        sizePolicy3.setHeightForWidth(objectListView->sizePolicy().hasHeightForWidth());
        objectListView->setSizePolicy(sizePolicy3);
        objectListView->setEditTriggers(QAbstractItemView::NoEditTriggers);

        verticalLayout_3->addWidget(objectListView);

        tabWidget_2->addTab(tab_6, QString());

        verticalLayout->addWidget(tabWidget_2);

        tabWidget = new QTabWidget(widget2);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        sizePolicy1.setHeightForWidth(tabWidget->sizePolicy().hasHeightForWidth());
        tabWidget->setSizePolicy(sizePolicy1);
        tabWidget->setMinimumSize(QSize(20, 29));
        tabWidget->setMaximumSize(QSize(16777215, 16777215));
        tabWidget->setSizeIncrement(QSize(0, 0));
        tabWidget->setTabPosition(QTabWidget::North);
        tabWidget->setMovable(true);
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        QSizePolicy sizePolicy4(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(tab->sizePolicy().hasHeightForWidth());
        tab->setSizePolicy(sizePolicy4);
        horizontalLayout_2 = new QHBoxLayout(tab);
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        console = new QPlainTextEdit(tab);
        console->setObjectName(QString::fromUtf8("console"));
        sizePolicy3.setHeightForWidth(console->sizePolicy().hasHeightForWidth());
        console->setSizePolicy(sizePolicy3);
        console->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        console->setBackgroundVisible(false);

        horizontalLayout_2->addWidget(console);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        label_2 = new QLabel(tab_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(11, 1, 44, 17));
        label_5 = new QLabel(tab_2);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(61, 24, 16, 16));
        label_3 = new QLabel(tab_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(61, 1, 16, 16));
        label_4 = new QLabel(tab_2);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(11, 24, 23, 17));
        label_6 = new QLabel(tab_2);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(11, 47, 18, 17));
        label_7 = new QLabel(tab_2);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(61, 47, 16, 16));
        tabWidget->addTab(tab_2, QString());
        tab_5 = new QWidget();
        tab_5->setObjectName(QString::fromUtf8("tab_5"));
        gridLayout_3 = new QGridLayout(tab_5);
        gridLayout_3->setSpacing(0);
        gridLayout_3->setContentsMargins(0, 0, 0, 0);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        doubleSpinBox = new QDoubleSpinBox(tab_5);
        doubleSpinBox->setObjectName(QString::fromUtf8("doubleSpinBox"));
        doubleSpinBox->setMinimum(-999);
        doubleSpinBox->setMaximum(999.99);

        gridLayout_3->addWidget(doubleSpinBox, 3, 2, 1, 1);

        doubleSpinBox_2 = new QDoubleSpinBox(tab_5);
        doubleSpinBox_2->setObjectName(QString::fromUtf8("doubleSpinBox_2"));
        doubleSpinBox_2->setMinimum(-999);
        doubleSpinBox_2->setMaximum(999.99);

        gridLayout_3->addWidget(doubleSpinBox_2, 3, 3, 1, 1);

        doubleSpinBox_3 = new QDoubleSpinBox(tab_5);
        doubleSpinBox_3->setObjectName(QString::fromUtf8("doubleSpinBox_3"));
        doubleSpinBox_3->setMinimum(-999);
        doubleSpinBox_3->setMaximum(999.99);

        gridLayout_3->addWidget(doubleSpinBox_3, 3, 4, 1, 1);

        label = new QLabel(tab_5);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy5(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy5.setHorizontalStretch(0);
        sizePolicy5.setVerticalStretch(1);
        sizePolicy5.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy5);

        gridLayout_3->addWidget(label, 3, 1, 1, 1);

        label_8 = new QLabel(tab_5);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        sizePolicy5.setHeightForWidth(label_8->sizePolicy().hasHeightForWidth());
        label_8->setSizePolicy(sizePolicy5);

        gridLayout_3->addWidget(label_8, 4, 1, 1, 1);

        doubleSpinBox_4 = new QDoubleSpinBox(tab_5);
        doubleSpinBox_4->setObjectName(QString::fromUtf8("doubleSpinBox_4"));
        doubleSpinBox_4->setMinimum(-4);
        doubleSpinBox_4->setMaximum(4);
        doubleSpinBox_4->setSingleStep(0.02);

        gridLayout_3->addWidget(doubleSpinBox_4, 4, 2, 1, 1);

        doubleSpinBox_5 = new QDoubleSpinBox(tab_5);
        doubleSpinBox_5->setObjectName(QString::fromUtf8("doubleSpinBox_5"));
        doubleSpinBox_5->setMinimum(-4);
        doubleSpinBox_5->setMaximum(4);
        doubleSpinBox_5->setSingleStep(0.02);

        gridLayout_3->addWidget(doubleSpinBox_5, 4, 3, 1, 1);

        doubleSpinBox_6 = new QDoubleSpinBox(tab_5);
        doubleSpinBox_6->setObjectName(QString::fromUtf8("doubleSpinBox_6"));
        doubleSpinBox_6->setMinimum(-4);
        doubleSpinBox_6->setMaximum(4);
        doubleSpinBox_6->setSingleStep(0.02);

        gridLayout_3->addWidget(doubleSpinBox_6, 4, 4, 1, 1);

        label_9 = new QLabel(tab_5);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        QSizePolicy sizePolicy6(QSizePolicy::MinimumExpanding, QSizePolicy::Maximum);
        sizePolicy6.setHorizontalStretch(0);
        sizePolicy6.setVerticalStretch(1);
        sizePolicy6.setHeightForWidth(label_9->sizePolicy().hasHeightForWidth());
        label_9->setSizePolicy(sizePolicy6);

        gridLayout_3->addWidget(label_9, 2, 1, 1, 1);

        lineEdit = new QLineEdit(tab_5);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));

        gridLayout_3->addWidget(lineEdit, 2, 2, 1, 3);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_3->addItem(verticalSpacer_2, 7, 1, 1, 1);

        colorbutton = new TerrificCoder::TCColorButton(tab_5);
        colorbutton->setObjectName(QString::fromUtf8("colorbutton"));
        colorbutton->setColor(QColor(0, 0, 0));
        colorbutton->setDefaultColor(QColor(0, 0, 0));
        colorbutton->setAlphaChannelEnabled(true);

        gridLayout_3->addWidget(colorbutton, 5, 2, 1, 3);

        label_10 = new QLabel(tab_5);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        gridLayout_3->addWidget(label_10, 5, 1, 1, 1);

        label_11 = new QLabel(tab_5);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        gridLayout_3->addWidget(label_11, 0, 1, 1, 1);

        label_12 = new QLabel(tab_5);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        gridLayout_3->addWidget(label_12, 0, 2, 1, 3);

        tabWidget->addTab(tab_5, QString());
        tab_7 = new QWidget();
        tab_7->setObjectName(QString::fromUtf8("tab_7"));
        gridLayout_2 = new QGridLayout(tab_7);
        gridLayout_2->setSpacing(0);
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        label_13 = new QLabel(tab_7);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        gridLayout_2->addWidget(label_13, 0, 0, 1, 1);

        colorbutton_2 = new TerrificCoder::TCColorButton(tab_7);
        colorbutton_2->setObjectName(QString::fromUtf8("colorbutton_2"));
        colorbutton_2->setColor(QColor(0, 0, 0));
        colorbutton_2->setDefaultColor(QColor(0, 0, 0));
        colorbutton_2->setAlphaChannelEnabled(true);

        gridLayout_2->addWidget(colorbutton_2, 0, 1, 1, 1);

        label_14 = new QLabel(tab_7);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        gridLayout_2->addWidget(label_14, 1, 0, 1, 1);

        colorbutton_3 = new TerrificCoder::TCColorButton(tab_7);
        colorbutton_3->setObjectName(QString::fromUtf8("colorbutton_3"));
        colorbutton_3->setColor(QColor(0, 0, 0));
        colorbutton_3->setDefaultColor(QColor(0, 0, 0));
        colorbutton_3->setAlphaChannelEnabled(true);

        gridLayout_2->addWidget(colorbutton_3, 1, 1, 1, 1);

        label_16 = new QLabel(tab_7);
        label_16->setObjectName(QString::fromUtf8("label_16"));

        gridLayout_2->addWidget(label_16, 2, 0, 1, 1);

        colorbutton_5 = new TerrificCoder::TCColorButton(tab_7);
        colorbutton_5->setObjectName(QString::fromUtf8("colorbutton_5"));
        colorbutton_5->setColor(QColor(0, 0, 0));
        colorbutton_5->setDefaultColor(QColor(0, 0, 0));
        colorbutton_5->setAlphaChannelEnabled(true);

        gridLayout_2->addWidget(colorbutton_5, 2, 1, 1, 1);

        label_18 = new QLabel(tab_7);
        label_18->setObjectName(QString::fromUtf8("label_18"));

        gridLayout_2->addWidget(label_18, 3, 0, 1, 1);

        colorbutton_6 = new TerrificCoder::TCColorButton(tab_7);
        colorbutton_6->setObjectName(QString::fromUtf8("colorbutton_6"));
        colorbutton_6->setColor(QColor(0, 0, 0));
        colorbutton_6->setDefaultColor(QColor(0, 0, 0));
        colorbutton_6->setAlphaChannelEnabled(true);

        gridLayout_2->addWidget(colorbutton_6, 3, 1, 1, 1);

        label_17 = new QLabel(tab_7);
        label_17->setObjectName(QString::fromUtf8("label_17"));

        gridLayout_2->addWidget(label_17, 4, 0, 1, 1);

        doubleSpinBox_7 = new QDoubleSpinBox(tab_7);
        doubleSpinBox_7->setObjectName(QString::fromUtf8("doubleSpinBox_7"));
        doubleSpinBox_7->setMinimum(-1);
        doubleSpinBox_7->setMaximum(1);

        gridLayout_2->addWidget(doubleSpinBox_7, 4, 1, 1, 1);

        doubleSpinBox_8 = new QDoubleSpinBox(tab_7);
        doubleSpinBox_8->setObjectName(QString::fromUtf8("doubleSpinBox_8"));
        doubleSpinBox_8->setMinimum(-1);
        doubleSpinBox_8->setMaximum(1);

        gridLayout_2->addWidget(doubleSpinBox_8, 4, 2, 1, 1);

        doubleSpinBox_9 = new QDoubleSpinBox(tab_7);
        doubleSpinBox_9->setObjectName(QString::fromUtf8("doubleSpinBox_9"));
        doubleSpinBox_9->setMinimum(-1);
        doubleSpinBox_9->setMaximum(1);
        doubleSpinBox_9->setValue(0);

        gridLayout_2->addWidget(doubleSpinBox_9, 4, 3, 1, 1);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer_3, 7, 1, 1, 1);

        label_15 = new QLabel(tab_7);
        label_15->setObjectName(QString::fromUtf8("label_15"));

        gridLayout_2->addWidget(label_15, 5, 0, 1, 1);

        label_19 = new QLabel(tab_7);
        label_19->setObjectName(QString::fromUtf8("label_19"));

        gridLayout_2->addWidget(label_19, 6, 0, 1, 1);

        doubleSpinBox_10 = new QDoubleSpinBox(tab_7);
        doubleSpinBox_10->setObjectName(QString::fromUtf8("doubleSpinBox_10"));
        doubleSpinBox_10->setMaximum(360);
        doubleSpinBox_10->setSingleStep(0.1);

        gridLayout_2->addWidget(doubleSpinBox_10, 5, 1, 1, 1);

        doubleSpinBox_11 = new QDoubleSpinBox(tab_7);
        doubleSpinBox_11->setObjectName(QString::fromUtf8("doubleSpinBox_11"));
        doubleSpinBox_11->setMaximum(360);
        doubleSpinBox_11->setSingleStep(0.1);

        gridLayout_2->addWidget(doubleSpinBox_11, 6, 1, 1, 1);

        tabWidget->addTab(tab_7, QString());

        verticalLayout->addWidget(tabWidget);


        gridLayout->addWidget(widget2, 0, 0, 1, 1);

        glwidget = new TerrificCoder::TCGLWidget(terrificmodellingview_base);
        glwidget->setObjectName(QString::fromUtf8("glwidget"));
        QSizePolicy sizePolicy7(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy7.setHorizontalStretch(4);
        sizePolicy7.setVerticalStretch(1);
        sizePolicy7.setHeightForWidth(glwidget->sizePolicy().hasHeightForWidth());
        glwidget->setSizePolicy(sizePolicy7);
        glwidget->setMinimumSize(QSize(1040, 780));
        glwidget->setMaximumSize(QSize(2048, 1152));
        glwidget->setSizeIncrement(QSize(16, 9));
        glwidget->setBaseSize(QSize(1040, 780));
        glwidget->setCursor(QCursor(Qt::CrossCursor));
        glwidget->setMouseTracking(true);
        glwidget->setAutoFillBackground(false);
        glwidget->setStyleSheet(QString::fromUtf8(""));

        gridLayout->addWidget(glwidget, 0, 1, 1, 1);


        retranslateUi(terrificmodellingview_base);
        QObject::connect(pushButton_6, SIGNAL(clicked()), glwidget, SLOT(new_box()));
        QObject::connect(pushButton, SIGNAL(clicked()), glwidget, SLOT(new_cone()));
        QObject::connect(pushButton_2, SIGNAL(clicked()), glwidget, SLOT(new_sphere()));
        QObject::connect(pushButton_3, SIGNAL(clicked()), glwidget, SLOT(new_thorus()));
        QObject::connect(pushButton_4, SIGNAL(clicked()), glwidget, SLOT(new_cylinder()));
        QObject::connect(pushButton_5, SIGNAL(clicked()), glwidget, SLOT(new_camera()));
        QObject::connect(glwidget, SIGNAL(signal_camera_list(QAbstractItemModel*)), cameraListView, SLOT(updateModel(QAbstractItemModel*)));
        QObject::connect(glwidget, SIGNAL(signal_object_list(QAbstractItemModel*)), objectListView, SLOT(updateModel(QAbstractItemModel*)));
        QObject::connect(glwidget, SIGNAL(signal_serious(QString)), console, SLOT(appendHtml(QString)));
        QObject::connect(glwidget, SIGNAL(signal_verbose(QString)), console, SLOT(appendHtml(QString)));
        QObject::connect(glwidget, SIGNAL(signal_debug(QString)), console, SLOT(appendHtml(QString)));
        QObject::connect(glwidget, SIGNAL(signal_object_selected(QModelIndex)), objectListView, SLOT(setCurrentIndex(QModelIndex)));
        QObject::connect(objectListView, SIGNAL(clicked(QModelIndex)), glwidget, SLOT(object_selected(QModelIndex)));
        QObject::connect(pushButton_7, SIGNAL(clicked()), glwidget, SLOT(object_delete()));
        QObject::connect(doubleSpinBox, SIGNAL(valueChanged(double)), glwidget, SLOT(object_position_x(double)));
        QObject::connect(doubleSpinBox_2, SIGNAL(valueChanged(double)), glwidget, SLOT(object_position_y(double)));
        QObject::connect(doubleSpinBox_3, SIGNAL(valueChanged(double)), glwidget, SLOT(object_position_z(double)));
        QObject::connect(doubleSpinBox_4, SIGNAL(valueChanged(double)), glwidget, SLOT(object_rotation_x(double)));
        QObject::connect(doubleSpinBox_5, SIGNAL(valueChanged(double)), glwidget, SLOT(object_rotation_y(double)));
        QObject::connect(doubleSpinBox_6, SIGNAL(valueChanged(double)), glwidget, SLOT(object_rotation_z(double)));
        QObject::connect(lineEdit, SIGNAL(textChanged(QString)), glwidget, SLOT(object_name(QString)));
        QObject::connect(glwidget, SIGNAL(signal_object_color(QColor)), colorbutton, SLOT(updateColor(QColor)));
        QObject::connect(glwidget, SIGNAL(signal_object_position_x(double)), doubleSpinBox, SLOT(setValue(double)));
        QObject::connect(glwidget, SIGNAL(signal_object_position_y(double)), doubleSpinBox_2, SLOT(setValue(double)));
        QObject::connect(glwidget, SIGNAL(signal_object_position_z(double)), doubleSpinBox_3, SLOT(setValue(double)));
        QObject::connect(glwidget, SIGNAL(signal_object_rotation_x(double)), doubleSpinBox_4, SLOT(setValue(double)));
        QObject::connect(glwidget, SIGNAL(signal_object_rotation_y(double)), doubleSpinBox_5, SLOT(setValue(double)));
        QObject::connect(glwidget, SIGNAL(signal_object_rotation_z(double)), doubleSpinBox_6, SLOT(setValue(double)));
        QObject::connect(glwidget, SIGNAL(signal_object_name(QString)), lineEdit, SLOT(setText(QString)));
        QObject::connect(glwidget, SIGNAL(signal_object_type(QString)), label_12, SLOT(setText(QString)));
        QObject::connect(colorbutton, SIGNAL(changed(QColor)), glwidget, SLOT(object_color(QColor)));
        QObject::connect(pushButton_8, SIGNAL(clicked()), glwidget, SLOT(new_light()));
        QObject::connect(colorbutton_2, SIGNAL(changed(QColor)), glwidget, SLOT(light_ambient(QColor)));
        QObject::connect(colorbutton_3, SIGNAL(changed(QColor)), glwidget, SLOT(light_specular(QColor)));
        QObject::connect(colorbutton_5, SIGNAL(changed(QColor)), glwidget, SLOT(light_diffuse(QColor)));
        QObject::connect(colorbutton_6, SIGNAL(changed(QColor)), glwidget, SLOT(light_spot(QColor)));
        QObject::connect(doubleSpinBox_9, SIGNAL(valueChanged(double)), glwidget, SLOT(light_spot_direction_z(double)));
        QObject::connect(doubleSpinBox_8, SIGNAL(valueChanged(double)), glwidget, SLOT(light_spot_direction_y(double)));
        QObject::connect(doubleSpinBox_7, SIGNAL(valueChanged(double)), glwidget, SLOT(light_spot_direction_x(double)));
        QObject::connect(glwidget, SIGNAL(signal_light_ambient(QColor)), colorbutton_2, SLOT(updateColor(QColor)));
        QObject::connect(glwidget, SIGNAL(signal_light_specular(QColor)), colorbutton_3, SLOT(updateColor(QColor)));
        QObject::connect(glwidget, SIGNAL(signal_light_diffuse(QColor)), colorbutton_5, SLOT(updateColor(QColor)));
        QObject::connect(glwidget, SIGNAL(signal_light_spot(QColor)), colorbutton_6, SLOT(updateColor(QColor)));
        QObject::connect(glwidget, SIGNAL(signal_light_spot_direction_z(double)), doubleSpinBox_9, SLOT(setValue(double)));
        QObject::connect(glwidget, SIGNAL(signal_light_spot_direction_y(double)), doubleSpinBox_8, SLOT(setValue(double)));
        QObject::connect(glwidget, SIGNAL(signal_light_spot_direction_x(double)), doubleSpinBox_7, SLOT(setValue(double)));
        QObject::connect(doubleSpinBox_11, SIGNAL(valueChanged(double)), glwidget, SLOT(light_spot_exponent(double)));
        QObject::connect(doubleSpinBox_10, SIGNAL(valueChanged(double)), glwidget, SLOT(light_spot_cutoff(double)));
        QObject::connect(glwidget, SIGNAL(signal_light_spot_cutoff(double)), doubleSpinBox_10, SLOT(setValue(double)));
        QObject::connect(glwidget, SIGNAL(signal_light_spot_exponent(double)), doubleSpinBox_11, SLOT(setValue(double)));

        tabWidget_2->setCurrentIndex(2);
        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(terrificmodellingview_base);
    } // setupUi

    void retranslateUi(QWidget *terrificmodellingview_base)
    {
        terrificmodellingview_base->setWindowTitle(tr2i18n("kapp4_base", 0));
        pushButton_6->setText(tr2i18n("Box", 0));
        pushButton->setText(tr2i18n("Cone", 0));
        pushButton_2->setText(tr2i18n("Sphere", 0));
        pushButton_3->setText(tr2i18n("Thorus", 0));
        pushButton_4->setText(tr2i18n("Cylinder", 0));
        pushButton_8->setText(tr2i18n("Light", 0));
        pushButton_5->setText(tr2i18n("Camera", 0));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_3), tr2i18n(" New Item", 0));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_4), tr2i18n("Camera List", 0));
        pushButton_7->setText(tr2i18n("Delete", 0));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_6), tr2i18n("Object List", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab), tr2i18n("Console", 0));
        label_2->setText(tr2i18n("Center", 0));
        label_5->setText(QString());
        label_3->setText(QString());
        label_4->setText(tr2i18n("Eye", 0));
        label_6->setText(tr2i18n("Up", 0));
        label_7->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab_2), tr2i18n("Camera", 0));
        doubleSpinBox->setPrefix(tr2i18n("x ", 0));
        doubleSpinBox_2->setPrefix(tr2i18n("y ", 0));
        doubleSpinBox_3->setPrefix(tr2i18n("z ", 0));
        label->setText(tr2i18n("Position", 0));
        label_8->setText(tr2i18n("Rotation", 0));
        doubleSpinBox_4->setPrefix(tr2i18n("x ", 0));
        doubleSpinBox_5->setPrefix(tr2i18n("y ", 0));
        doubleSpinBox_6->setPrefix(tr2i18n("z ", 0));
        label_9->setText(tr2i18n("Name", 0));
        label_10->setText(tr2i18n("Color", 0));
        label_11->setText(tr2i18n("Type", 0));
        label_12->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab_5), tr2i18n("Object", 0));
        label_13->setText(tr2i18n("Ambient", 0));
        label_14->setText(tr2i18n("Specular", 0));
        label_16->setText(tr2i18n("Diffuse", 0));
        label_18->setText(tr2i18n("Spot", 0));
        colorbutton_6->setText(tr2i18n("Spot", 0));
        label_17->setText(tr2i18n("Spot Direction", 0));
        doubleSpinBox_7->setPrefix(tr2i18n("x ", 0));
        doubleSpinBox_8->setPrefix(tr2i18n("y ", 0));
        doubleSpinBox_9->setPrefix(tr2i18n("z ", 0));
        label_15->setText(tr2i18n("Spot Cutoff", 0));
        label_19->setText(tr2i18n("Spot Exponent", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_7), tr2i18n("Light", 0));
    } // retranslateUi

};

namespace Ui {
    class terrificmodellingview_base: public Ui_terrificmodellingview_base {};
} // namespace Ui

QT_END_NAMESPACE

#endif // TERRIFICMODELLINGVIEW_BASE_H

