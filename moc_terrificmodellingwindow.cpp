/****************************************************************************
** Meta object code from reading C++ file 'terrificmodellingwindow.h'
**
** Created: Fri Dec 24 10:58:43 2010
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../projects/terrificmodelling/src/terrificmodellingwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'terrificmodellingwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_TerrificCoder__TerrificModellingWindow[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_TerrificCoder__TerrificModellingWindow[] = {
    "TerrificCoder::TerrificModellingWindow\0"
};

const QMetaObject TerrificCoder::TerrificModellingWindow::staticMetaObject = {
    { &KXmlGuiWindow::staticMetaObject, qt_meta_stringdata_TerrificCoder__TerrificModellingWindow,
      qt_meta_data_TerrificCoder__TerrificModellingWindow, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TerrificCoder::TerrificModellingWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TerrificCoder::TerrificModellingWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TerrificCoder::TerrificModellingWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TerrificCoder__TerrificModellingWindow))
        return static_cast<void*>(const_cast< TerrificModellingWindow*>(this));
    if (!strcmp(_clname, "Ui::terrificmodelling_window"))
        return static_cast< Ui::terrificmodelling_window*>(const_cast< TerrificModellingWindow*>(this));
    return KXmlGuiWindow::qt_metacast(_clname);
}

int TerrificCoder::TerrificModellingWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KXmlGuiWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
