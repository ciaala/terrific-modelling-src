/****************************************************************************
** Meta object code from reading C++ file 'tcglwidget.h'
**
** Created: Fri Dec 24 10:58:43 2010
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../projects/terrificmodelling/src/terrificcoder/tcglwidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'tcglwidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_TerrificCoder__TCGLWidget[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      49,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      28,       // signalCount

 // signals: signature, parameters, type, tag, flags
      34,   27,   26,   26, 0x05,
      56,   27,   26,   26, 0x05,
      80,   27,   26,   26, 0x05,
     104,   27,   26,   26, 0x05,
     134,   27,   26,   26, 0x05,
     161,   27,   26,   26, 0x05,
     193,  187,   26,   26, 0x05,
     233,  187,   26,   26, 0x05,
     279,  273,   26,   26, 0x05,
     321,  315,   26,   26, 0x05,
     354,  315,   26,   26, 0x05,
     387,  315,   26,   26, 0x05,
     420,  315,   26,   26, 0x05,
     453,  315,   26,   26, 0x05,
     486,  315,   26,   26, 0x05,
     524,  519,   26,   26, 0x05,
     557,  552,   26,   26, 0x05,
     591,  585,   26,   26, 0x05,
     619,  585,   26,   26, 0x05,
     648,  585,   26,   26, 0x05,
     677,  585,   26,   26, 0x05,
     707,  585,   26,   26, 0x05,
     733,  315,   26,   26, 0x05,
     768,  315,   26,   26, 0x05,
     801,  315,   26,   26, 0x05,
     839,  315,   26,   26, 0x05,
     877,  315,   26,   26, 0x05,
     915,  187,   26,   26, 0x05,

 // slots: signature, parameters, type, tag, flags
     967,  960,   26,   26, 0x0a,
    1005,  273,   26,   26, 0x0a,
    1034,   26,   26,   26, 0x0a,
    1050,  315,   26,   26, 0x0a,
    1076,  315,   26,   26, 0x0a,
    1102,  315,   26,   26, 0x0a,
    1128,  315,   26,   26, 0x0a,
    1154,  315,   26,   26, 0x0a,
    1180,  315,   26,   26, 0x0a,
    1206,  552,   26,   26, 0x0a,
    1227,  585,   26,   26, 0x0a,
    1248,  585,   26,   26, 0x0a,
    1270,  585,   26,   26, 0x0a,
    1292,  585,   26,   26, 0x0a,
    1315,  585,   26,   26, 0x0a,
    1334,  315,   26,   26, 0x0a,
    1362,  315,   26,   26, 0x0a,
    1388,  315,   26,   26, 0x0a,
    1419,  315,   26,   26, 0x0a,
    1450,  315,   26,   26, 0x0a,
    1481,   26,   26,   26, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_TerrificCoder__TCGLWidget[] = {
    "TerrificCoder::TCGLWidget\0\0string\0"
    "signal_debug(QString)\0signal_verbose(QString)\0"
    "signal_serious(QString)\0"
    "signal_camera_center(QString)\0"
    "signal_camera_eye(QString)\0"
    "signal_camera_up(QString)\0model\0"
    "signal_camera_list(QAbstractItemModel*)\0"
    "signal_object_list(QAbstractItemModel*)\0"
    "index\0signal_object_selected(QModelIndex)\0"
    "value\0signal_object_position_x(double)\0"
    "signal_object_position_y(double)\0"
    "signal_object_position_z(double)\0"
    "signal_object_rotation_x(double)\0"
    "signal_object_rotation_y(double)\0"
    "signal_object_rotation_z(double)\0type\0"
    "signal_object_type(QString)\0name\0"
    "signal_object_name(QString)\0color\0"
    "signal_object_color(QColor)\0"
    "signal_light_ambient(QColor)\0"
    "signal_light_diffuse(QColor)\0"
    "signal_light_specular(QColor)\0"
    "signal_light_spot(QColor)\0"
    "signal_light_spot_exponent(double)\0"
    "signal_light_spot_cutoff(double)\0"
    "signal_light_spot_direction_x(double)\0"
    "signal_light_spot_direction_y(double)\0"
    "signal_light_spot_direction_z(double)\0"
    "signal_object_treemodel(QAbstractItemModel*)\0"
    "action\0toolbar_newobject_triggered(QAction*)\0"
    "object_selected(QModelIndex)\0"
    "object_delete()\0object_position_x(double)\0"
    "object_position_y(double)\0"
    "object_position_z(double)\0"
    "object_rotation_x(double)\0"
    "object_rotation_y(double)\0"
    "object_rotation_z(double)\0"
    "object_name(QString)\0object_color(QColor)\0"
    "light_ambient(QColor)\0light_diffuse(QColor)\0"
    "light_specular(QColor)\0light_spot(QColor)\0"
    "light_spot_exponent(double)\0"
    "light_spot_cutoff(double)\0"
    "light_spot_direction_x(double)\0"
    "light_spot_direction_y(double)\0"
    "light_spot_direction_z(double)\0"
    "setupSceneModel()\0"
};

const QMetaObject TerrificCoder::TCGLWidget::staticMetaObject = {
    { &QGLWidget::staticMetaObject, qt_meta_stringdata_TerrificCoder__TCGLWidget,
      qt_meta_data_TerrificCoder__TCGLWidget, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TerrificCoder::TCGLWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TerrificCoder::TCGLWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TerrificCoder::TCGLWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TerrificCoder__TCGLWidget))
        return static_cast<void*>(const_cast< TCGLWidget*>(this));
    if (!strcmp(_clname, "TCGui"))
        return static_cast< TCGui*>(const_cast< TCGLWidget*>(this));
    return QGLWidget::qt_metacast(_clname);
}

int TerrificCoder::TCGLWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGLWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: signal_debug((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: signal_verbose((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: signal_serious((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: signal_camera_center((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: signal_camera_eye((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 5: signal_camera_up((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 6: signal_camera_list((*reinterpret_cast< QAbstractItemModel*(*)>(_a[1]))); break;
        case 7: signal_object_list((*reinterpret_cast< QAbstractItemModel*(*)>(_a[1]))); break;
        case 8: signal_object_selected((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 9: signal_object_position_x((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 10: signal_object_position_y((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 11: signal_object_position_z((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 12: signal_object_rotation_x((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 13: signal_object_rotation_y((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 14: signal_object_rotation_z((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 15: signal_object_type((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 16: signal_object_name((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 17: signal_object_color((*reinterpret_cast< QColor(*)>(_a[1]))); break;
        case 18: signal_light_ambient((*reinterpret_cast< QColor(*)>(_a[1]))); break;
        case 19: signal_light_diffuse((*reinterpret_cast< QColor(*)>(_a[1]))); break;
        case 20: signal_light_specular((*reinterpret_cast< QColor(*)>(_a[1]))); break;
        case 21: signal_light_spot((*reinterpret_cast< QColor(*)>(_a[1]))); break;
        case 22: signal_light_spot_exponent((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 23: signal_light_spot_cutoff((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 24: signal_light_spot_direction_x((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 25: signal_light_spot_direction_y((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 26: signal_light_spot_direction_z((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 27: signal_object_treemodel((*reinterpret_cast< QAbstractItemModel*(*)>(_a[1]))); break;
        case 28: toolbar_newobject_triggered((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 29: object_selected((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 30: object_delete(); break;
        case 31: object_position_x((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 32: object_position_y((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 33: object_position_z((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 34: object_rotation_x((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 35: object_rotation_y((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 36: object_rotation_z((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 37: object_name((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 38: object_color((*reinterpret_cast< QColor(*)>(_a[1]))); break;
        case 39: light_ambient((*reinterpret_cast< QColor(*)>(_a[1]))); break;
        case 40: light_diffuse((*reinterpret_cast< QColor(*)>(_a[1]))); break;
        case 41: light_specular((*reinterpret_cast< QColor(*)>(_a[1]))); break;
        case 42: light_spot((*reinterpret_cast< QColor(*)>(_a[1]))); break;
        case 43: light_spot_exponent((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 44: light_spot_cutoff((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 45: light_spot_direction_x((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 46: light_spot_direction_y((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 47: light_spot_direction_z((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 48: setupSceneModel(); break;
        default: ;
        }
        _id -= 49;
    }
    return _id;
}

// SIGNAL 0
void TerrificCoder::TCGLWidget::signal_debug(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void TerrificCoder::TCGLWidget::signal_verbose(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void TerrificCoder::TCGLWidget::signal_serious(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void TerrificCoder::TCGLWidget::signal_camera_center(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void TerrificCoder::TCGLWidget::signal_camera_eye(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void TerrificCoder::TCGLWidget::signal_camera_up(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void TerrificCoder::TCGLWidget::signal_camera_list(QAbstractItemModel * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void TerrificCoder::TCGLWidget::signal_object_list(QAbstractItemModel * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void TerrificCoder::TCGLWidget::signal_object_selected(QModelIndex _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void TerrificCoder::TCGLWidget::signal_object_position_x(double _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void TerrificCoder::TCGLWidget::signal_object_position_y(double _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}

// SIGNAL 11
void TerrificCoder::TCGLWidget::signal_object_position_z(double _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 11, _a);
}

// SIGNAL 12
void TerrificCoder::TCGLWidget::signal_object_rotation_x(double _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 12, _a);
}

// SIGNAL 13
void TerrificCoder::TCGLWidget::signal_object_rotation_y(double _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 13, _a);
}

// SIGNAL 14
void TerrificCoder::TCGLWidget::signal_object_rotation_z(double _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 14, _a);
}

// SIGNAL 15
void TerrificCoder::TCGLWidget::signal_object_type(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 15, _a);
}

// SIGNAL 16
void TerrificCoder::TCGLWidget::signal_object_name(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 16, _a);
}

// SIGNAL 17
void TerrificCoder::TCGLWidget::signal_object_color(QColor _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 17, _a);
}

// SIGNAL 18
void TerrificCoder::TCGLWidget::signal_light_ambient(QColor _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 18, _a);
}

// SIGNAL 19
void TerrificCoder::TCGLWidget::signal_light_diffuse(QColor _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 19, _a);
}

// SIGNAL 20
void TerrificCoder::TCGLWidget::signal_light_specular(QColor _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 20, _a);
}

// SIGNAL 21
void TerrificCoder::TCGLWidget::signal_light_spot(QColor _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 21, _a);
}

// SIGNAL 22
void TerrificCoder::TCGLWidget::signal_light_spot_exponent(double _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 22, _a);
}

// SIGNAL 23
void TerrificCoder::TCGLWidget::signal_light_spot_cutoff(double _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 23, _a);
}

// SIGNAL 24
void TerrificCoder::TCGLWidget::signal_light_spot_direction_x(double _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 24, _a);
}

// SIGNAL 25
void TerrificCoder::TCGLWidget::signal_light_spot_direction_y(double _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 25, _a);
}

// SIGNAL 26
void TerrificCoder::TCGLWidget::signal_light_spot_direction_z(double _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 26, _a);
}

// SIGNAL 27
void TerrificCoder::TCGLWidget::signal_object_treemodel(QAbstractItemModel * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 27, _a);
}
QT_END_MOC_NAMESPACE
