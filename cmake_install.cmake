# Install script for directory: /home/crypt/projects/terrificmodelling/src

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/home/crypt/local")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "DebugFull")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "0")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  IF(EXISTS "$ENV{DESTDIR}/home/crypt/local/bin/terrificmodelling" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/crypt/local/bin/terrificmodelling")
    FILE(RPATH_CHECK
         FILE "$ENV{DESTDIR}/home/crypt/local/bin/terrificmodelling"
         RPATH "/home/crypt/local/lib64:/usr/lib64/qt4")
  ENDIF()
  list(APPEND CPACK_ABSOLUTE_DESTINATION_FILES
   "/home/crypt/local/bin/terrificmodelling")
FILE(INSTALL DESTINATION "/home/crypt/local/bin" TYPE EXECUTABLE FILES "/home/crypt/Documents/src/terrificmodelling")
  IF(EXISTS "$ENV{DESTDIR}/home/crypt/local/bin/terrificmodelling" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/crypt/local/bin/terrificmodelling")
    FILE(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/home/crypt/local/bin/terrificmodelling"
         OLD_RPATH "/usr/lib64/qt4::::::::::::::::::::::::"
         NEW_RPATH "/home/crypt/local/lib64:/usr/lib64/qt4")
    IF(CMAKE_INSTALL_DO_STRIP)
      EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/home/crypt/local/bin/terrificmodelling")
    ENDIF(CMAKE_INSTALL_DO_STRIP)
  ENDIF()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CPACK_ABSOLUTE_DESTINATION_FILES
   "/home/crypt/local/share/applications/kde4/terrificmodelling.desktop")
FILE(INSTALL DESTINATION "/home/crypt/local/share/applications/kde4" TYPE FILE FILES "/home/crypt/projects/terrificmodelling/src/terrificmodelling.desktop")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CPACK_ABSOLUTE_DESTINATION_FILES
   "/home/crypt/local/share/config.kcfg/terrificmodelling.kcfg")
FILE(INSTALL DESTINATION "/home/crypt/local/share/config.kcfg" TYPE FILE FILES "/home/crypt/projects/terrificmodelling/src/terrificmodelling.kcfg")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CPACK_ABSOLUTE_DESTINATION_FILES
   "/home/crypt/local/share/apps/terrificmodelling/terrificmodellingui.rc")
FILE(INSTALL DESTINATION "/home/crypt/local/share/apps/terrificmodelling" TYPE FILE FILES "/home/crypt/projects/terrificmodelling/src/terrificmodellingui.rc")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  INCLUDE("/home/crypt/Documents/src/terrificcoder/cmake_install.cmake")
  INCLUDE("/home/crypt/Documents/src/test/cmake_install.cmake")

ENDIF(NOT CMAKE_INSTALL_LOCAL_ONLY)

